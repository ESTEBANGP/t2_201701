package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private int lista;
	private NodoSencillo<T> nodo;
	private NodoSencillo<T> actual;

	public ListaEncadenada() 
	{
		lista = 0;
		nodo = null;
		actual = nodo;
	}
	@Override
	public Iterator<T> iterator() {

		return new Iterator<T>() {

			public boolean hasNext()
			{
				if (actual == null)
				{
					return false;
				}
				else if (actual.darNext()== null)
				{
					return false;
				}
				else{
					return true;
				}
			}

			@Override
			public T next() {
				T temp = actual.darItem();
				actual = actual.darNext();
				return temp;
			}

			@Override
			public void remove() {
			}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) 
	{
		NodoSencillo<T> nodoNuevo = new NodoSencillo<T>(elem);
		if(nodo == null)
		{
			nodo = nodoNuevo;
		}
		else
		{
			actual = nodo;
			while (actual.darNext()!=null) 
			{
				actual = actual.darNext();
			}
			actual.cambiarItem(elem);
		}
		lista++;
	}

	@Override
	public T darElemento(int pos)
	{
		int contador = 0;
		if(lista == 0)
		{
			return null;
		}
		while(iterator().hasNext() && contador!=pos)
		{
			nodo = nodo.darNext();
			contador ++;
		}
		return nodo.darItem();

	}


	@Override
	public int darNumeroElementos() 
	{
		return lista;
	}

	@Override
	public T darElementoPosicionActual() 
	{
		return actual.darItem();
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		boolean a = false;
		if (iterator().hasNext())
		{
			actual = actual.darNext();
			a = true;
		}
		else if(actual.darNext() == null )
		{
			a = false;
		}

		return a;
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{
		boolean back = false;
		while (iterator().hasNext())
		{
			if (actual == actual.darNext())
			{
				back = true;
			}
		}

		return back;	
	}

}
