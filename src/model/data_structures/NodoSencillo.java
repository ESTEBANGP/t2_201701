package model.data_structures;


public class NodoSencillo <Item>
{
private NodoSencillo<Item> next;
	
	private Item item;
	
	public NodoSencillo (Item e)
	{
		next = null;
		item = e;
	}

	public void cambiarSiguiente(NodoSencillo<Item> a)
	{
		this.next = a;
	}
	
	public NodoSencillo<Item> darNext()
	{
		return next;
	}
	
	public void cambiarItem(Item elem)
	{
		this.item = elem;
	}
	
	public Item darItem()
	{
		return item;
	}
}
