package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

		private int tamaņoLista;
		private NodoDoble<T> primero;
		private NodoDoble<T> ultimo;
		private NodoDoble<T> actual;
		
		public ListaDobleEncadenada(){
			tamaņoLista=0;
			primero=null;
			ultimo=null;
			actual=primero;
		}
	
		@Override
		public Iterator<T> iterator() {
			// TODO Auto-generated method stub
		     return new IteratorListaDoble<T>(primero);
		}
	
		@Override
		public void agregarElementoFinal(T elem) {
			// TODO Auto-generated method stub
			Iterator<T> iterador = iterator();
			
			if(!iterador.hasNext()){
				primero= new NodoDoble<T>(elem,0);
				ultimo= primero;
				actual=primero;
				tamaņoLista++;
			}
			else{
			
			NodoDoble<T> elAgregar= new NodoDoble<T>(elem, ultimo.darPosicion()+1);
			ultimo.modificarSiguiente(elAgregar, elAgregar.darPosicion());
			elAgregar.modificarAnterior(ultimo, ultimo.darPosicion());
			ultimo=elAgregar;
			tamaņoLista++;
			}
		}
	
		@Override
		public T darElemento(int pos) {
			// TODO Auto-generated method stub
			Iterator<T> iterador = iterator();
			
			while (iterador.hasNext()){
			  	
				if (actual.darPosicion()==pos){
			  		return actual.darElemento();
			  	}
			  	
			  	iterador.next();
			}
			
			return null;
		}
	
	
		@Override
		public int darNumeroElementos() {
			// TODO Auto-generated method stub
			return tamaņoLista;
		}
	
		@Override
		public T darElementoPosicionActual() {
			// TODO Auto-generated method stub
			return actual.darElemento();
		}
	
		@Override
		public boolean avanzarSiguientePosicion() {
			// TODO Auto-generated method stub
			Iterator<T> iterador = iterator();
			
			if (!iterador.hasNext()){
				return false;
			}
			else{
				if (actual.darSiguiente()==null ){
					return false;
				}
				else{
					actual=actual.darSiguiente();
					return true;
				}
			}
			
			
		}
		public void moverActualInicio(){
		
				actual=primero;
			
		}
		public int referenciarPrimero(){
			return primero.darPosicion();
		}
		public void moverIzquierda(){
			if (primero!=null && actual.darAnterior()!=null && actual!=primero){
				actual=actual.darAnterior();
			}
		}
	
		@Override
		public boolean retrocederPosicionAnterior() {
			// TODO Auto-generated method stub
			Iterator<T> iterador = iterator();
			if (!iterador.hasNext()){
				return false;
			}
			else{
				if (actual.darAnterior()==null ){
					return false;
				}
				else{
					actual=actual.darAnterior();
					return true;
				}
			}
			
		}



}
